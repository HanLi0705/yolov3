set(_CATKIN_CURRENT_PACKAGE "yolo_network")
set(yolo_network_VERSION "0.0.0")
set(yolo_network_MAINTAINER "hanli <hanli@todo.todo>")
set(yolo_network_PACKAGE_FORMAT "2")
set(yolo_network_BUILD_DEPENDS "rospy")
set(yolo_network_BUILD_EXPORT_DEPENDS "rospy")
set(yolo_network_BUILDTOOL_DEPENDS "catkin")
set(yolo_network_BUILDTOOL_EXPORT_DEPENDS )
set(yolo_network_EXEC_DEPENDS "rospy")
set(yolo_network_RUN_DEPENDS "rospy")
set(yolo_network_TEST_DEPENDS )
set(yolo_network_DOC_DEPENDS )
set(yolo_network_URL_WEBSITE "")
set(yolo_network_URL_BUGTRACKER "")
set(yolo_network_URL_REPOSITORY "")
set(yolo_network_DEPRECATED "")