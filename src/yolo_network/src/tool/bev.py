import numpy as np
import os
np.set_printoptions(threshold=np.inf)
import matplotlib.pyplot as plt
import math
import pcl
#from scipy.misc import imsave
from more_itertools import chunked
import matplotlib.pyplot as plt
import time
#import numba
#from numba import jit
def remove_points(point_cloud, bc):
    min_x = bc['minX']
    max_x = bc['maxX']
    min_y = bc['minY']
    max_y = bc['maxY']
    min_z = bc['minZ']
    max_z = bc['maxZ']
    mask = np.where((point_cloud[:, 0] >= min_x) & (point_cloud[:, 0] <= max_x)
                    & (point_cloud[:, 1] >= min_y) & (point_cloud[:, 1] <= max_y)
                    & (point_cloud[:, 2] >= min_z) & (point_cloud[:, 2] <= max_z))
    point_cloud = point_cloud[mask]
    point_cloud[:, 2] = point_cloud[:, 2] + 2
    return point_cloud


def make_bv_feature(point_cloud_):
    Height = 768+1
    Width = 768+1
    point_cloud = np.copy(point_cloud_) 
    point_cloud[:, 0] = np.int_(np.floor(point_cloud_[:, 0] / 60.0 * 768))
    point_cloud[:, 1] = np.int_(np.floor(point_cloud_[:, 1] / 30.0 * 384) + 384)
    indices = np.lexsort(
        (-point_cloud[:, 2], point_cloud[:, 0], point_cloud[:, 1]))
    #print(indices)
    point_cloud = point_cloud[indices]
    
    #print(point_cloud)
    #print(len(point_cloud))
    # Height Map
    height_map = np.zeros((Height, Width))
    #print(len(height_map))
    #_, indices = np.unique(point_cloud[:, 0:2], axis=0, return_index=True)
    #print(len(indices))
    #point_cloud_frac = point_cloud[indices]
    #print(point_cloud_frac)
    # some important problem is image coordinate is (y,x), not (x,y)
    #height_map[np.int_(point_cloud_frac[:, 0]),
               #np.int_(point_cloud_frac[:, 1])] = point_cloud_frac[:, 2]
    # Intensity Map & DensityMap
    intensity_map = np.zeros((Height, Width))
    density_map = np.zeros((Height, Width))
    _, indices, counts = np.unique(point_cloud[:, 0:2],
                                   axis=0,
                                   return_index=True,
                                   return_counts=True)
    #print(len(counts))
    point_cloud_top = point_cloud[indices]
    #print(point_cloud_top.shape)
    counts1=counts*pow(pow(point_cloud_top[:,0]-384,2)+pow((point_cloud_top[:,1]),2),0.5)
    #print(np.max(np.log(counts1 + 1)))
    #print(np.min(np.log(counts1 + 1)))
    normalized_counts = np.minimum(1.0, (np.log(counts1 + 1)-3) / 6)
    #print(normalized_counts)
    #list5=[]
    #for i in range (len(normalized_counts)):
       # list5.append(i)
    #plt.scatter(list5,normalized_counts,alpha=0.01)
    #plt.show()
    #height_map[np.int_(point_cloud_top[:, 0]),np.int_(point_cloud_top[:, 1])] = point_cloud_top[:, 3]*pow(pow(point_cloud_top[:,0]-384,2)+pow((point_cloud_top[:,1]),2),0.5)
    #print(point_cloud_top[:, 3])
    density_map[np.int_(point_cloud_top[:, 0]),
                np.int_(point_cloud_top[:, 1])] = normalized_counts
   # print(density_map[np.int_(point_cloud_top[:, 0]),
                #np.int_(point_cloud_top[:, 1])].shape)
    #print(normalized_counts.shape)
    list1=[]
    list2=[]
    list3=[]
    count=0
    #print())
    #print(len(point_cloud))
    for i in counts:
        #print(i) 
        list1.append(np.mean(point_cloud[count:count+i,2]))
        list2.append(np.sum(point_cloud[count:count+i,3]))
        #list2.append(np.mean(point_cloud[count:count+i,3]))
        #if np.var(point_cloud[count:count+i,2]) <= 1:
            #list3.append(pow((1-pow((np.var(point_cloud[count:count+i,2])-1),8)),0.125))
        #else:
            #list3.append(1)
        count+=i

    #print(list1)
    #print(np.min(list2))
    #print(np.max(list2))
    #list3= pow((1-pow((list2/max(list2)-1),2)),0.5)
    list3= np.array(list2)/counts
    #print(len(list2))
    height_map[np.int_(point_cloud_top[:, 0]), 
               np.int_(point_cloud_top[:, 1])] = list1
    print(len(counts))
    print(len(list2))
    intensity_map[np.int_(point_cloud_top[:, 0]),
               np.int_(point_cloud_top[:, 1])] = list3
    #print(point_cloud_frac[:, 2])
    #plt.scatter(counts,list2,alpha=0.01)
    #plt.show()
    rgb_map = np.zeros((Height, Width,3))
    height_map=height_map/3.25
    #print(np.max(height_map))
    #height_map=height_map/np.max(np.max(height_map))
    rgb_map[:, :, 1] = density_map  # r_map
    rgb_map[:, :, 2] = height_map   # g_map
    rgb_map[:, :, 0] = intensity_map  # b_map
    #RGB_Map = np.zeros((3,Height,Width))
   # RGB_Map[0, :, :]=rgb_map[: ,:, 0]
    #RGB_Map[1, :, :] = rgb_map[:, :, 1]
    #RGB_Map[2, :, :] = rgb_map[:, :, 2]
    #print(normalized_counts)
    save = np.zeros((768, 768, 3))
    #save = np.zeros((3,768,768))
    #save = RGB_Map[:,0:768, 0:768]
    save = rgb_map[0:768, 0:768,:]
    return save



velodyne = "/1/"
bin = '/home/hanli/Documents/Liangdao/script/ros_ws/src/yolo_network/src/tool/'

bc={}
bc['minX'] = -60; bc['maxX'] = 60; bc['minY'] = -30.0; bc['maxY'] = 30.0
bc['minZ'] =-2; bc['maxZ'] = 1.25
start = time.clock()
for i in range(1):
    filename = bin +str(i).zfill(6)+".bin"
    print("Processing:",filename)
    PointCloud = np.fromfile(filename,dtype=np.float32)
    point_cloud = PointCloud.reshape((-1,4))
    #np.savetxt(str(i).zfill(6)+".csv", point_cloud, delimiter = ',')
    b=remove_points(point_cloud,bc)
    bird_view = make_bv_feature(b)
    
    #bird_view.tofile("/home/liangdao/Downloads/Yolov3-master/data/KITTI/object/training/BEV/"+str(i).zfill(6)+".bin")
    #np.save("/media/liangdao/DATA/Yolov3-master/data/KITTI/object/training/BEV/"+str(i).zfill(6)+".npy",bird_view)
    i = 0
    plt.imshow(np.rot90(bird_view, 2).copy())
    
    plt.savefig("/home/hanli/Documents/Liangdao/script/ros_ws/src/yolo_network/src/tool/bothside.png")
end=time.clock()
print(str(end-start))

