#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  6 15:08:07 2019

@author: lihan
"""
import sys
#when you run test show img,uncomment the next line

import utils.config as cnf
import utils.kitti_bev_utils as bev_utils

import pcl
import numpy as np
from matplotlib import pyplot as plt
import torch
from torch.autograd import Variable
import time
from models import *
import argparse
import utils.utils as utils
from jsk_recognition_msgs.msg import BoundingBox, BoundingBoxArray
#from jsk_topic_tools import ConnectionBasedTransport
'''
pcd = np.asarray(pcl.load('/home/lihan/Documents/Liangdao/script/Yolov3-master/park.pcd'))

temp = np.copy(pcd[:,0])
pcd[:,0] = pcd[:,2]
pcd[:,1] = -pcd[:,1]
pcd[:,2] = temp

'''
#kitty = np.load('/home/lihan/Documents/Liangdao/script/Yolov3-master/data/000005inno.npy')
#inno = np.load('/home/lihan/Documents/Liangdao/script/Yolov3-master/data/000011kitty.npy')
'''
TODO calibration points clouds
'''
'''
init model TODO: argparse .cfg
'''


    
    
def main(pcd,model,opt,boxes):    
    start = time.time()
    
    #rgb convert
    
    #temp = np.copy(pcd[:,0])
    #pcd[:,0] = pcd[:,2]
    
    #pcd[:,1] = -pcd[:,1]
    #pcd[:,2] = temp
    

    #print('number of points: ',pcd.shape)
    pcd_fiter = bev_utils.removePoints(pcd,cnf.boundary)
    print('after filter number of points: ',pcd_fiter.shape)
    rgb_map = bev_utils.makeBVFeature(pcd_fiter, cnf.DISCRETIZATION, cnf.boundary)
    #rgb_map = rgb_map.swapaxes(0,2)
    
    #plt.imshow(rgb_map)
    
    '''
    if cuda to cuda.FloatTensor
    '''
    
    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
    input_img = Variable(torch.from_numpy(rgb_map).type(Tensor))
    input_img = input_img.unsqueeze(0)

    '''
    boundingbox array, boundingbox
    '''
    
    
    
    with torch.no_grad():
        detections = model(input_img)
        #print(detections)
        detections = utils.non_max_suppression_rotated_bbox(detections, opt.conf_thres, opt.nms_thres) 
        

    
    end = time.time()
    print(end-start)
    
    img_detections = []  # Stores detections for each image index
    img_detections.extend(detections)
    
    bev_maps = torch.squeeze(input_img).numpy()
    
    RGB_Map = np.zeros((cnf.BEV_WIDTH, cnf.BEV_WIDTH, 3))
    RGB_Map[:, :, 2] = bev_maps[0, :, :]  # r_map
    RGB_Map[:, :, 1] = bev_maps[1, :, :]  # g_map
    RGB_Map[:, :, 0] = bev_maps[2, :, :]  # b_map
    
    RGB_Map *= 255
    RGB_Map = RGB_Map.astype(np.uint8)
    
    for detections in img_detections:
        if detections is None:
            box = BoundingBox()
            #box.header = boxes.header
#            boxes.boxes.append([])
            print(boxes)
            print(None)
            return boxes
            continue
    
        # Rescale boxes to original image
        #print(detections)
        detections = utils.rescale_boxes(detections, opt.img_size, RGB_Map.shape[:2])
        
        #bbox = []
        
        
        '''
        TODO if box is None return ....
        '''        
        
        for x, y, w, l, im, re, conf, cls_conf, cls_pred in detections.numpy():
            # boundingbox
            yaw = np.arctan2(im, re)
            
            
            box = BoundingBox()
            box.header = boxes.header
            
            box.pose.position.x = x # center x
            box.pose.position.y = y # center y
            box.pose.position.z = 0 # center z
            
            box.pose.orientation.w = np.cos(yaw/2) #ori w
            box.pose.orientation.x = 0 
            box.pose.orientation.y = 0
            box.pose.orientation.z = np.sin(yaw/2) #ori z
            
            box.dimensions.x = w #bbox width
            box.dimensions.y = l #bbox lang
            box.dimensions.z = 0.5 #bbox height
            
            box.value = cls_conf #confident
            box.label = np.int(cls_pred) # label
            
            boxes.boxes.append(box)
            
            #print(cls_pred)
            # Draw rotated box
            #bev_utils.drawRotatedBox(RGB_Map, x, y, w, l, yaw, cnf.colors[int(cls_pred)])
            #bbox_each =[x,y,w,l,yaw,cls_pred]
            #bbox.append(bbox_each)
    plt.imshow(RGB_Map)
    
    #print(bbox)
    #print(boxes)
    return boxes
if __name__ == '__main__':
    
    PathName ='/home/lihan/Documents/Liangdao/script/Yolov3-master/data/000254.bin'
    #pcd = np.asarray(pcl.load('/home/lihan/Documents/Liangdao/script/Yolov3-master/park.pcd'))
    pcd = np.asarray(pcl.load('/home/lihan/Documents/pcd/1566219987.791807000.pcd'))
    #pcd = []
    #pcd.append(np.fromfile(PathName,dtype=np.float32).reshape(-1,4))
    #pcd = np.array(pcd[0][:,0:3].astype(np.float32))


    parser = argparse.ArgumentParser()
    parser.add_argument("--model_def", type=str, default="config/complex_yolov3.cfg", help="path to model definition file")
    parser.add_argument("--weights_path", type=str, default="checkpoints/1.pth", help="path to weights file")
    parser.add_argument("--class_path", type=str, default="data/classes.names", help="path to class label file")
    parser.add_argument("--conf_thres", type=float, default=0.8, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.4, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--img_size", type=int, default=cnf.BEV_WIDTH, help="size of each image dimension")
    parser.add_argument("--split", type=str, default="valid", help="text file having image lists in dataset")
    parser.add_argument("--folder", type=str, default="training", help="directory name that you downloaded all dataset")
    opt = parser.parse_args()
    end = time.time()
    
    #classes = utils.load_classes(opt.class_path)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    
    model =  Darknet(opt.model_def, img_size=opt.img_size).to(device)
    model.load_state_dict(torch.load(opt.weights_path,map_location={'cuda:0': 'cpu'}))
    model.eval()
    boxes = BoundingBoxArray()
    bbox1 = main(pcd,model,opt,boxes)
#    print(bbox1[0])
    
    



