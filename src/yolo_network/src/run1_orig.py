#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  6 15:08:07 2019

@author: lihan
"""
import sys
#when you run test show img,uncomment the next line

import utils.config as cnf
import utils.kitti_bev_utils as bev_utils

import pcl
import numpy as np
from matplotlib import pyplot as plt
import torch
from torch.autograd import Variable
import time
from models import *
import argparse
import utils.utils as utils
sys.path.append('/opt/ros/kinetic/lib/python2.7/dist-packages')
from jsk_recognition_msgs.msg import BoundingBox, BoundingBoxArray
#from jsk_topic_tools import ConnectionBasedTransport
'''
pcd = np.asarray(pcl.load('/home/lihan/Documents/Liangdao/script/Yolov3-master/park.pcd'))

temp = np.copy(pcd[:,0])
pcd[:,0] = pcd[:,2]
pcd[:,1] = -pcd[:,1]
pcd[:,2] = temp

'''
#kitty = np.load('/home/lihan/Documents/Liangdao/script/Yolov3-master/data/000005inno.npy')
#inno = np.load('/home/lihan/Documents/Liangdao/script/Yolov3-master/data/000011kitty.npy')
'''
TODO calibration points clouds
'''
'''
init model TODO: argparse .cfg
'''


    
    
def main(pcd,model,opt,boxes,device):    
    start = time.time()
    
    #rgb convert
    
    #temp = np.copy(pcd[:,0])
    #pcd[:,0] = pcd[:,2]
    
    #pcd[:,1] = -pcd[:,1]
    #pcd[:,2] = temp
    

    #print('number of points: ',pcd.shape)
    pcd_fiter = bev_utils.removePoints(pcd,cnf.boundary)
    print('after filter number of points: ',pcd_fiter.shape)
    rgb_map = bev_utils.makeBVFeature(pcd_fiter, cnf.DISCRETIZATION, cnf.boundary)
    
    '''
    add back view
    '''
    rgb_map = bev_utils.makeBVFeature(pcd_fiter, cnf.DISCRETIZATION, cnf.boundary_back)
    #rgb_map = rgb_map.swapaxes(0,2)
    #rgb_map reverse 180 degree
    '''
    rgb_map = rgb_map_old.copy()
    rgb_map[0,:,:] = np.rot90(rgb_map_old[0,:,:],2).copy()
    rgb_map[1,:,:] = np.rot90(rgb_map_old[1,:,:],2).copy()
    rgb_map[2,:,:] = np.rot90(rgb_map_old[2,:,:],2).copy()
    print(rgb_map.shape)
    '''
    #plt.imshow(rgb_map)
    
    '''
    if cuda to cuda.FloatTensor
    '''
    
    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
    
    
    input_img = Variable(torch.from_numpy(rgb_map).type(Tensor))
    input_img = input_img.unsqueeze(0)
    
    '''
    add back view
    '''
    input_img_back = Variable(torch.from_numpy(rgb_map).type(Tensor))
    input_img_back = input_img_back.unsqueeze(0)

    '''
    boundingbox array, boundingbox
    '''
    
    
    
    with torch.no_grad():
        detections = model(input_img)
        detections_back = model(input_img)
        print('shape : {}'.format(detections.shape))
        #num_bbox_front = 
        #print(detections)
        detections = utils.non_max_suppression_rotated_bbox(detections, opt.conf_thres, opt.nms_thres) 
        

    
    end = time.time()
    print(end-start)
    
    img_detections = []  # Stores detections for each image index
    img_detections.extend(detections)
    '''
    bev_maps = torch.squeeze(input_img.cpu()).numpy()
    
    RGB_Map = np.zeros((cnf.BEV_WIDTH, cnf.BEV_WIDTH, 3))
    RGB_Map[:, :, 2] = bev_maps[0, :, :]  # r_map
    RGB_Map[:, :, 1] = bev_maps[1, :, :]  # g_map
    RGB_Map[:, :, 0] = bev_maps[2, :, :]  # b_map
    
    RGB_Map *= 255
    RGB_Map = RGB_Map.astype(np.uint8)
    '''
    for detections in img_detections:
        if detections is None:
            box = BoundingBox()
            #box.header = boxes.header
#            boxes.boxes.append([])
            print(boxes)
            print(None)
            return boxes
            continue
    
        # Rescale boxes to original image
        #print(detections)
        #detections = utils.rescale_boxes(detections, opt.img_size, RGB_Map.shape[:2])

        #bbox = []


        
        '''
        TODO if box is None return ....
        '''        
        
        
        '''
        TODO reduce loop and add conf
        '''
        
        predictions = np.zeros([len(detections), 8], dtype=np.float32)
        count = 0





        for x, y, w, l, im, re, conf, cls_conf, cls_pred in detections.numpy():
            # boundingbox
            yaw = np.arctan2(im, re)
            predictions[count, :] = cls_pred, x/cnf.BEV_WIDTH, y/cnf.BEV_WIDTH, w/cnf.BEV_WIDTH, l/cnf.BEV_WIDTH, im, re,conf
            
            print('timesample : {}'.format(boxes.header.stamp))
            print('boundingBox confidence : {}'.format(conf))
            print('x,y: {}'.format((x,y)))
            count += 1


        #predictions1 : inverse the coordinate
        
        '''
        delect the too closed bbox
        '''
        predictions1 = bev_utils.inverse_yolo_target(predictions, cnf.boundary)

        if len(predictions1) <= 1:
            pass
        else:
            position=predictions1[:,1:3] #bbox coordinate
            distance = np.linalg.norm(position-position[:,None],axis=-1)
            print('distance')
            print(distance)
            bbox_closed = np.array(np.where((distance<.8)&(distance>0.0)))
            if np.any(bbox_closed) == 0:
                pass
            else:
                _,indice = np.unique(bbox_closed.sum(axis=0),axis=0,return_index=True)
                print(indice)
                print('befor{}'.format(len(predictions1)))
                bbb = predictions1[bbox_closed[:,indice],-1]
                print(bbox_closed[:,indice])

                print(bbb.min(axis=0))

                predictions1 = np.delete(predictions1,bbox_closed[:,indice][bbb.argmin(axis=0)],axis=0)
                print('after{}'.format(len(predictions1)))

        for c, x, y, z, h, w, l, yaw, conf in predictions1:
        
            box = BoundingBox()
            box.header = boxes.header
            
            box.pose.position.x = x # center x
            box.pose.position.y = y # center y
            box.pose.position.z = z # center z
            
            box.pose.orientation.w = np.cos(yaw/2) #ori w
            box.pose.orientation.x = 0 
            box.pose.orientation.y = 0
            box.pose.orientation.z = np.sin(yaw/2) #ori z
            
            box.dimensions.x = l #bbox width
            box.dimensions.y = w #bbox lang
            box.dimensions.z = h #bbox height
            
            #box.value = c #confident
            box.label = np.int(c) # label
            
            boxes.boxes.append(box)
            
            #print(cls_pred)
            # Draw rotated box
            #bev_utils.drawRotatedBox(RGB_Map, x, y, w, l, yaw, cnf.colors[0])
            
            #bbox_each =[x,y,w,l,yaw,cls_pred]
            #bbox.append(bbox_each)

        
    
    #print(bbox)
    #print(boxes)
    return boxes
if __name__ == '__main__':
    
    PathName ='/home/lihan/Documents/Liangdao/script/Yolov3-master/data/000254.bin'
    #pcd = np.asarray(pcl.load('/home/lihan/Documents/Liangdao/script/Yolov3-master/park.pcd'))
    pcd = np.asarray(pcl.load('/home/lihan/Documents/pcd/1566219987.791807000.pcd'))
    #pcd = []
    #pcd.append(np.fromfile(PathName,dtype=np.float32).reshape(-1,4))
    #pcd = np.array(pcd[0][:,0:3].astype(np.float32))


    parser = argparse.ArgumentParser()
    parser.add_argument("--model_def", type=str, default="config/complex_yolov3.cfg", help="path to model definition file")
    parser.add_argument("--weights_path", type=str, default="checkpoints/2.pth", help="path to weights file")
    parser.add_argument("--class_path", type=str, default="data/classes.names", help="path to class label file")
    parser.add_argument("--conf_thres", type=float, default=0.8, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.9, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--img_size", type=int, default=cnf.BEV_WIDTH, help="size of each image dimension")
    parser.add_argument("--split", type=str, default="valid", help="text file having image lists in dataset")
    parser.add_argument("--folder", type=str, default="training", help="directory name that you downloaded all dataset")
    opt = parser.parse_args()
    end = time.time()
    
    #classes = utils.load_classes(opt.class_path)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    
    model =  Darknet(opt.model_def, img_size=opt.img_size).to(device)
    model.load_state_dict(torch.load(opt.weights_path,map_location={'cuda:0': 'cpu'}))
    model.eval()
    boxes = BoundingBoxArray()
    bbox1 = main(pcd,model,opt,boxes)
#    print(bbox1[0])
    
    



