#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  6 15:08:07 2019

@author: lihan
"""
#Subscribe
import pickle as cPickle
import sys
#sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
sys.path.append('/opt/ros/kinetic/lib/python2.7/dist-packages')
import numpy as np
import rospy
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import PointCloud2,PointField
import torch
from torch.autograd import Variable
import time
import utils.config as cnf
from models import *
import argparse
import utils.utils as utils
from run1 import main
from jsk_recognition_msgs.msg import BoundingBox, BoundingBoxArray
import jsk_recognition_msgs
#from jsk_topic_tools import ConnectionBasedTransport

def callback(data):
    print('!')
    assert isinstance(data,PointCloud2)
    #read iv_points x,y,z
    #gen = pc2.read_points(data, field_names=("x", "y", "z",'i'), skip_nans=True)
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_def", type=str, default="config/complex_yolov3.cfg", help="path to model definition file")
    parser.add_argument("--weights_path", type=str, default="checkpoints/hanli.pth", help="path to weights file")
    parser.add_argument("--class_path", type=str, default="data/classes.names", help="path to class label file")
    parser.add_argument("--conf_thres", type=float, default=0.8, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.04, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--img_size", type=int, default=cnf.BEV_WIDTH, help="size of each image dimension")
    parser.add_argument("--split", type=str, default="valid", help="text file having image lists in dataset")
    parser.add_argument("--folder", type=str, default="training", help="directory name that you downloaded all dataset")
    opt = parser.parse_args()
    
    '''
    init boundingbox,boundingboxarray
    '''
    
    boxes = BoundingBoxArray()
    boxes.header = data.header
    #print(data.header)
    gen = pc2.read_points(data)
    #print(list(gen))
    #print(type(list(gen)[0]))
    points=np.array(list(gen)).reshape((1,-1))
    points=points.reshape((-1,4))
    points[:,3] = np.round(points[:,3]/255,decimals=2)
    pointXYZ = points[:,:4]
    
    
    '''   
    for p in gen:
        points.append(p[0])
        points.append(p[1])
        points.append(p[2])
        print(p[3])

    points = np.array(points).reshape((-1,3)).astype(np.float32)
    
    
    #to pointcloud2------------------------------------------
    cloud_msg = PointCloud2()
    cloud_msg.header = data.header
    #print(points.shape)
    #print(data.height,data.width)
    #print(len(data.data))
    #print(data.fields)
    
    pointXYZI= np.column_stack((pointXYZ,points[:,4]))
    

    PointXYZI_to_list = [tuple(x) for x in pointXYZI.tolist()]

    
    
    print(PointXYZI_to_list[0])
    
    cloud_msg.data = np.array(PointXYZI_to_list).tostring()
    cloud_msg.height = 1
    cloud_msg.width = pointXYZ.shape[0]
    cloud_msg.fields = [PointField("x", 0, 7, 1),
                        PointField("y", 4, 7, 1),
                        PointField("z", 8, 7, 1),
                        PointField("intensity",12,2,1)]
    #print(cloud_msg.fields)
    cloud_msg.is_bigendian = data.is_bigendian # assumption
    cloud_msg.point_step = data.point_step
    cloud_msg.row_step =  data.row_step
    cloud_msg.is_dense = data.is_dense
    
    #print(cloud_msg.width,len(cloud_msg.data))
    print(list(gen1))
    
    #-------------------------------------------------------
    '''
    bbox = main(pointXYZ,model,opt,boxes,device)
    #print(bbox)
    #print(type(bbox))
    pub.publish(bbox)
    #kitt.publish(cloud_msg)
    #rospy.loginfo(bbox)
        
    '''
    publisher
    '''
    
    

#    rospy.loginfo(rospy.get_caller_id()+': {}'.format(points.shape[0])+'\n'+'{}'.format(bbox))
    
    

    
if __name__=='__main__':
#    main()
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_def", type=str, default="config/complex_yolov3.cfg", help="path to model definition file")
    parser.add_argument("--weights_path", type=str, default="checkpoints/hanli.pth", help="path to weights file")
    parser.add_argument("--class_path", type=str, default="data/classes.names", help="path to class label file")
    parser.add_argument("--conf_thres", type=float, default=0.8, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.04, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--img_size", type=int, default=cnf.BEV_WIDTH, help="size of each image dimension")
    parser.add_argument("--split", type=str, default="valid", help="text file having image lists in dataset")
    parser.add_argument("--folder", type=str, default="training", help="directory name that you downloaded all dataset")
    opt = parser.parse_args()
        
    #classes = utils.load_classes(opt.class_path)
     
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    print(opt)
    model =  Darknet(opt.model_def, img_size=opt.img_size).to(device)
    model.load_state_dict(torch.load(opt.weights_path,map_location={'cuda:0': 'cpu'}))
    model.eval()
    print('success')
    rospy.init_node('yolo_network',anonymous = True)
    global pub,rate
    #pub = ConnectionBasedTransport.advertise('jsk_Boundingbox', BoundingBoxArray,queue_size=1)
    pub = rospy.Publisher('jsk_Boundingbox', BoundingBoxArray,queue_size=1)
    rate = rospy.Rate(10)
    #kitt = rospy.Publisher('Kitt', PointCloud2,queue_size=10)
    #sub = rospy.Subscriber('/iv_points1',PointCloud2,callback)
    sub = rospy.Subscriber('/kitti/velo/pointcloud',PointCloud2,callback)
    rospy.spin()
    #print(pub)
    
