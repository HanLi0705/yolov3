from __future__ import division
import os
import glob
import numpy as np
import cv2
import torch.utils.data as torch_data
import utils.kitti_utils as kitti_utils
#from PIL import Image
import matplotlib.pyplot as plt
import os
np.set_printoptions(threshold=np.inf)

class KittiDataset(torch_data.Dataset):

    def __init__(self, root_dir, split='train', folder='testing'):
        self.split = split

        is_test = self.split == 'test'
        self.imageset_dir = os.path.join(root_dir, 'KITTI', 'object', folder)
        self.lidar_path = os.path.join(self.imageset_dir, "velodyne")

        self.image_path = os.path.join(self.imageset_dir, "image_2")
        self.calib_path = os.path.join(self.imageset_dir, "calib")
        self.label_path = os.path.join(self.imageset_dir, "label_2")
        self.BEV_path = os.path.join(self.imageset_dir, "BEV")

        if not is_test:
            split_dir = os.path.join('data', 'KITTI', 'ImageSets', split+'.txt')
            self.image_idx_list = [x.strip() for x in open(split_dir).readlines()]
        else:
            self.files = sorted(glob.glob("%s/*.bin" % self.lidar_path))
            self.image_idx_list = [os.path.split(x)[1].split(".")[0].strip() for x in self.files]
            #print(self.image_idx_list[0])

        self.num_samples = self.image_idx_list.__len__()
    def get_BEV(self, idx):
        BEV_file = os.path.join(self.BEV_path, '%06d.npy' % idx)
        assert os.path.exists(BEV_file)
        #im=Image.open(BEV_file)
        rgb = np.load(BEV_file)
        #rgb=np.array(cv2.imread(BEV_file))
        #RGB_Map = np.zeros((3, 768, 768))
        #RGB_Map[0, :, :]=rgb[:,:,0]
        #RGB_Map[1, :, :] = rgb[:, :, 1]
        #RGB_Map[2, :, :] = rgb[:, :, 2]
        return rgb # (H, W, C) -> (H, W, 3) OpenCV reads in BGR mode
    def get_image(self, idx):
        img_file = os.path.join(self.image_path, '%06d.png' % idx)
        assert os.path.exists(img_file)
        return cv2.imread(img_file) # (H, W, C) -> (H, W, 3) OpenCV reads in BGR mode

    def get_image_shape(self, idx):
        img_file = os.path.join(self.image_path, '%06d.png' % idx)
        assert  os.path.exists(img_file)
        img = cv2.imread(img_file)
        width, height, channel = img.shape
        return width, height, channel

    def get_lidar(self, idx):
        lidar_file = os.path.join(self.lidar_path, '%06d.bin' % idx)
        assert os.path.exists(lidar_file)
        return np.fromfile(lidar_file, dtype=np.float32).reshape(-1, 4)

    def get_calib(self, idx):
        calib_file = os.path.join(self.calib_path, '%06d.txt' % idx)
        assert os.path.exists(calib_file)
        return kitti_utils.Calibration(calib_file)

    def get_label(self, idx):
        label_file = os.path.join(self.label_path, '%06d.txt' % idx)
        assert os.path.exists(label_file)
        return kitti_utils.read_label(label_file)

    def __len__(self):
        raise NotImplemented

    def __getitem__(self, item):
        raise NotImplemented
